package com.mega.Excel;

import com.monitorjbl.xlsx.StreamingReader;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/**
 * @author Erinda Febrian - erindafebrian3@gmail.com
 * 30/09/2020
 * class name ExcelOld
 */


public class ExcelOld {
    static int countInsert = 0;

    static Long sum = (long) 0;
    static int sum2 = 0;
    static int sum3 = 0;

    static Long sum4 = (long) 0;
    static int sum5 = 0;
    static int sum6 = 0;

    static Long sum7 = (long) 0;
    static int sum8 = 0;
    static int sum9 = 0;

    static Long sum10 = (long) 0;
    static int sum11 = 0;
    static int sum12 = 0;

    static Long sum13 = (long) 0;
    static int sum14 = 0;
    static int sum15 = 0;

    static Long sum16 = (long) 0;
    static int sum17 = 0;
    static int sum18 = 0;
    static Long sum19 = (long) 0;
    static int sum20 = 0;
    static int sum21 = 0;
    static Long sum22 = (long) 0;
    static int sum23 = 0;
    static int sum24 = 0;
    static Long sum25 = (long) 0;
    static int sum26 = 0;
    static int sum27 = 0;
    static Long sum28 = (long) 0;
    static int sum29 = 0;
    static int sum30 = 0;

    static int i2 = 0;
    static ArrayList<Cell> value = new ArrayList<Cell>();

    static ArrayList<Long> sumlist = new ArrayList<Long>();
    static ArrayList<Integer> sumlist2 = new ArrayList<Integer>();
    static ArrayList<Integer> sumlist3 = new ArrayList<Integer>();

    static ArrayList<Long> sumlist4 = new ArrayList<Long>();
    static ArrayList<Integer> sumlist5 = new ArrayList<Integer>();
    static ArrayList<Integer> sumlist6 = new ArrayList<Integer>();

    static ArrayList<Long> sumlist7 = new ArrayList<Long>();
    static ArrayList<Integer> sumlist8 = new ArrayList<Integer>();
    static ArrayList<Integer> sumlist9 = new ArrayList<Integer>();

    static ArrayList<Long> sumlist10 = new ArrayList<Long>();
    static ArrayList<Integer> sumlist11 = new ArrayList<Integer>();
    static ArrayList<Integer> sumlist12 = new ArrayList<Integer>();

    static ArrayList<Long> sumlist13 = new ArrayList<Long>();
    static ArrayList<Integer> sumlist14 = new ArrayList<Integer>();
    static ArrayList<Integer> sumlist15 = new ArrayList<Integer>();

    static ArrayList<Long> sumlist16 = new ArrayList<Long>();
    static ArrayList<Integer> sumlist17 = new ArrayList<Integer>();
    static ArrayList<Integer> sumlist18 = new ArrayList<Integer>();

    static ArrayList<Long> sumlist19 = new ArrayList<Long>();
    static ArrayList<Integer> sumlist20 = new ArrayList<Integer>();
    static ArrayList<Integer> sumlist21 = new ArrayList<Integer>();

    static ArrayList<Long> sumlist22 = new ArrayList<Long>();
    static ArrayList<Integer> sumlist23 = new ArrayList<Integer>();
    static ArrayList<Integer> sumlist24 = new ArrayList<Integer>();

    static ArrayList<Long> sumlist25 = new ArrayList<Long>();
    static ArrayList<Integer> sumlist26 = new ArrayList<Integer>();
    static ArrayList<Integer> sumlist27 = new ArrayList<Integer>();

    static ArrayList<Long> sumlist28 = new ArrayList<Long>();
    static ArrayList<Integer> sumlist29 = new ArrayList<Integer>();
    static ArrayList<Integer> sumlist30 = new ArrayList<Integer>();

    public static void main(String args[]) throws Exception
    {
        File file = new File("CTOP - Data Transaksi & Fee Based ATM_Mei_2019_v1.0_RESUME (1).xlsx");
        System.out.println("Found : "+file);
        System.out.println("Cek file...");
        try (Workbook workbook = StreamingReader.builder()
                .rowCacheSize(100)
                .bufferSize(4096)
                .open(file)) {
            Sheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rowIt = sheet.rowIterator();
            while (rowIt.hasNext()) {
//    			Row currentRows = rowIt.next();
//
//    			if (currentRows.getRowNum() >= 2){
//					for (int i=4; i<33; i++) {
//						if (currentRows.getCell(i) == null) {
//							currentRows.createCell(i).setCellValue(0);
//						}
//					}
//				}

                Row currentRow = rowIt.next();
                if (currentRow.getRowNum() == 0 || currentRow.getRowNum() == 1) {
                    continue;
                } else {
                    extraxtData(currentRow);
                }
            }
            System.out.println("Cek file Berhasil");
        }
        List<Integer> listValue = new ArrayList<Integer>();
        value.forEach((item) -> listValue.add((int) item.getNumericCellValue()));
        List<Integer> sip = new ArrayList<Integer>(listValue);
        List<Integer> sipp = new ArrayList<Integer>(new HashSet<Integer>(sip));

        for (i2=0; i2<sipp.size(); i2++) {
            System.out.println("Proses..."+i2+" from "+(sipp.size()-1));
            try (Workbook workbook = StreamingReader.builder()
                    .rowCacheSize(100)
                    .bufferSize(4096)
                    .open(file)) {
                Sheet sheet = workbook.getSheetAt(0);
                Iterator<Row> rowIt = sheet.rowIterator();
                while (rowIt.hasNext()) {
                    Row currentRow = rowIt.next();
                    if (currentRow.getRowNum() == 0 || currentRow.getRowNum() == 1) {
                        continue;
                    } else {
                        extraxtData2(currentRow, sipp);
                    }
                }
                sumlist.add(sum);
                sum = (long) 0;
                sumlist2.add(sum2);
                sum2 = 0;
                sumlist3.add(sum3);
                sum3 = 0;
                sumlist4.add(sum4);
                sum4 = (long) 0;
                sumlist5.add(sum5);
                sum5 = 0;
                sumlist6.add(sum6);
                sum6 = 0;
                sumlist7.add(sum7);
                sum7 = (long) 0;
                sumlist8.add(sum8);
                sum8 = 0;
                sumlist9.add(sum9);
                sum9 = 0;
                sumlist10.add(sum10);
                sum10 = (long) 0;
                sumlist11.add(sum11);
                sum11 = 0;
                sumlist12.add(sum12);
                sum12 = 0;
                sumlist13.add(sum13);
                sum13 = (long) 0;
                sumlist14.add(sum14);
                sum14 = 0;
                sumlist15.add(sum15);
                sum15 = 0;
                sumlist16.add(sum16);
                sum16 = (long) 0;
                sumlist17.add(sum17);
                sum17 = 0;
                sumlist18.add(sum18);
                sum18 = 0;
                sumlist19.add(sum19);
                sum19 = (long) 0;
                sumlist20.add(sum20);
                sum20 = 0;
                sumlist21.add(sum21);
                sum21 = 0;
                sumlist22.add(sum22);
                sum22 = (long) 0;
                sumlist23.add(sum23);
                sum23 = 0;
                sumlist24.add(sum24);
                sum24 = 0;
                sumlist25.add(sum25);
                sum25 = (long) 0;
                sumlist26.add(sum26);
                sum26 = 0;
                sumlist27.add(sum27);
                sum27 = 0;
                sumlist28.add(sum28);
                sum28 = (long) 0;
                sumlist29.add(sum29);
                sum29 = 0;
                sumlist30.add(sum30);
                sum30 = 0;
            }
        }

        try {
            System.out.println("Meyimpan Data...");
            String filename = "NewExcelFile.xlsx" ;
            FileOutputStream fos = new FileOutputStream(filename);
            XSSFWorkbook workbooks = new XSSFWorkbook();
            XSSFSheet sheet = workbooks.createSheet("Sheet1");

            Row row = sheet.createRow(0);
            Cell cell0 = row.createCell(0);
            cell0.setCellValue("tlbtcd");
            Cell cell1 = row.createCell(1);
            cell1.setCellValue("Sum of amount_finals");
            Cell cell2 = row.createCell(2);
            cell2.setCellValue("Sum of Qty");
            Cell cell3 = row.createCell(3);
            cell3.setCellValue("Sum of fee_based");

            for(int i=0; i<sipp.size(); i++) {
                Row row1 = sheet.createRow(i+1);
                for(int j=0; j<4; j++) {
                    Cell cell11 = row1.createCell(j);
                    if (j==0) {
                        cell11.setCellValue(sipp.get(i));
                    }
                    else if (j==1) {
                        cell11.setCellValue(sumlist.get(i)+sumlist4.get(i)+sumlist7.get(i)+sumlist10.get(i)+sumlist13.get(i)+sumlist16.get(i)+sumlist19.get(i)+sumlist22.get(i)+sumlist25.get(i)+sumlist28.get(i));
                    }
                    else if (j==2) {
                        cell11.setCellValue(sumlist2.get(i)+sumlist5.get(i)+sumlist8.get(i)+sumlist11.get(i)+sumlist14.get(i)+sumlist17.get(i)+sumlist20.get(i)+sumlist23.get(i)+sumlist26.get(i)+sumlist29.get(i));
                    }
                    else if (j==3) {
                        cell11.setCellValue(sumlist3.get(i)+sumlist6.get(i)+sumlist9.get(i)+sumlist12.get(i)+sumlist15.get(i)+sumlist18.get(i)+sumlist21.get(i)+sumlist24.get(i)+sumlist27.get(i)+sumlist30.get(i));
                    }
                    /*
                     * else if (j==4) { cell11.setCellValue(sumlist4.get(i)); } else if (j==5) {
                     * cell11.setCellValue(sumlist5.get(i)); } else if (j==6) {
                     * cell11.setCellValue(sumlist6.get(i)); } else if (j==7) {
                     * cell11.setCellValue(sumlist7.get(i)); } else if (j==8) {
                     * cell11.setCellValue(sumlist8.get(i)); } else if (j==9) {
                     * cell11.setCellValue(sumlist9.get(i)); } else if (j==10) {
                     * cell11.setCellValue(sumlist10.get(i)); } else if (j==11) {
                     * cell11.setCellValue(sumlist11.get(i)); } else if (j==12) {
                     * cell11.setCellValue(sumlist12.get(i)); } else if (j==13) {
                     * cell11.setCellValue(sumlist13.get(i)); } else if (j==14) {
                     * cell11.setCellValue(sumlist14.get(i)); } else if (j==15) {
                     * cell11.setCellValue(sumlist15.get(i)); } else if (j==16) {
                     * cell11.setCellValue(sumlist16.get(i)); } else if (j==17) {
                     * cell11.setCellValue(sumlist17.get(i)); } else if (j==18) {
                     * cell11.setCellValue(sumlist18.get(i)); } else if (j==19) {
                     * cell11.setCellValue(sumlist19.get(i)); } else if (j==20) {
                     * cell11.setCellValue(sumlist20.get(i)); } else if (j==21) {
                     * cell11.setCellValue(sumlist21.get(i)); } else if (j==22) {
                     * cell11.setCellValue(sumlist22.get(i)); } else if (j==23) {
                     * cell11.setCellValue(sumlist23.get(i)); } else if (j==24) {
                     * cell11.setCellValue(sumlist24.get(i)); } else if (j==25) {
                     * cell11.setCellValue(sumlist25.get(i)); } else if (j==26) {
                     * cell11.setCellValue(sumlist26.get(i)); } else if (j==27) {
                     * cell11.setCellValue(sumlist28.get(i)); } else if (j==29) {
                     * cell11.setCellValue(sumlist29.get(i)); }
                     */
                }
            }
            workbooks.write(fos);
            fos.flush();
            fos.close();
            System.out.println("Data sudah tersimpan di file excel");
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void extraxtData(Row currentRow) {
        value.add(currentRow.getCell(2));
        for (int i=4; i<33; i++) {
            if (currentRow.getCell(i) == null) {
                currentRow.createCell(i).setCellValue(0);
            }
        }
    }
    public static void extraxtData2(Row currentRow, List<Integer> sipp) {
        Cell getcell = currentRow.getCell(2);
        int getcellvalue = (int) getcell.getNumericCellValue();
        Cell getcell2 = currentRow.getCell(4);
        Long getcellvalue2 = (long) getcell2.getNumericCellValue();
        Cell getcell3 = currentRow.getCell(5);
        int getcellvalue3 = (int) getcell3.getNumericCellValue();
        Cell getcell4 = currentRow.getCell(6);
        int getcellvalue4 = (int) getcell4.getNumericCellValue();
        Cell getcell5 = currentRow.getCell(7);
        Long getcellvalue5 = (long) getcell5.getNumericCellValue();
        Cell getcell6 = currentRow.getCell(8);
        int getcellvalue6 = (int) getcell6.getNumericCellValue();
        Cell getcell7 = currentRow.getCell(9);
        int getcellvalue7 = (int) getcell7.getNumericCellValue();
        Cell getcell8 = currentRow.getCell(10);
        Long getcellvalue8 = (long) getcell8.getNumericCellValue();
        Cell getcell9 = currentRow.getCell(11);
        int getcellvalue9 = (int) getcell9.getNumericCellValue();
        Cell getcell10 = currentRow.getCell(12);
        int getcellvalue10 = (int) getcell10.getNumericCellValue();
        Cell getcell11 = currentRow.getCell(13);
        Long getcellvalue11 = (long) getcell11.getNumericCellValue();
        Cell getcell12 = currentRow.getCell(14);
        int getcellvalue12 = (int) getcell12.getNumericCellValue();
        Cell getcell13 = currentRow.getCell(15);
        int getcellvalue13 = (int) getcell13.getNumericCellValue();
        Cell getcell14 = currentRow.getCell(16);
        Long getcellvalue14 = (long) getcell14.getNumericCellValue();
        Cell getcell15 = currentRow.getCell(17);
        int getcellvalue15 = (int) getcell15.getNumericCellValue();
        Cell getcell16 = currentRow.getCell(18);
        int getcellvalue16 = (int) getcell16.getNumericCellValue();
        Cell getcell17 = currentRow.getCell(19);
        Long getcellvalue17 = (long) getcell17.getNumericCellValue();
        Cell getcell18 = currentRow.getCell(20);
        int getcellvalue18 = (int) getcell18.getNumericCellValue();
        Cell getcell19 = currentRow.getCell(21);
        int getcellvalue19 = (int) getcell19.getNumericCellValue();
        Cell getcell20 = currentRow.getCell(22);
        Long getcellvalue20 = (long) getcell20.getNumericCellValue();
        Cell getcell21 = currentRow.getCell(23);
        int getcellvalue21 = (int) getcell21.getNumericCellValue();
        Cell getcell22 = currentRow.getCell(24);
        int getcellvalue22 = (int) getcell22.getNumericCellValue();
        Cell getcell23 = currentRow.getCell(25);
        Long getcellvalue23 = (long) getcell23.getNumericCellValue();
        Cell getcell24 = currentRow.getCell(26);
        int getcellvalue24 = (int) getcell24.getNumericCellValue();
        Cell getcell25 = currentRow.getCell(27);
        int getcellvalue25 = (int) getcell25.getNumericCellValue();
        Cell getcell26 = currentRow.getCell(28);
        Long getcellvalue26 = (long) getcell26.getNumericCellValue();
        Cell getcell27 = currentRow.getCell(29);
        int getcellvalue27 = (int) getcell27.getNumericCellValue();
        Cell getcell28 = currentRow.getCell(30);
        int getcellvalue28 = (int) getcell28.getNumericCellValue();
        Cell getcell29 = currentRow.getCell(31);
        Long getcellvalue29 = (long) getcell29.getNumericCellValue();
        Cell getcell30 = currentRow.getCell(32);
        int getcellvalue30 = (int) getcell30.getNumericCellValue();
        Cell getcell31 = currentRow.getCell(33);
        int getcellvalue31 = (int) getcell31.getNumericCellValue();
        if (sipp.get(i2) == getcellvalue) {
            sum = sum + getcellvalue2;
            sum2 = sum2 + getcellvalue3;
            sum3 = sum3 + getcellvalue4;
            sum4 = sum4 + getcellvalue5;
            sum5 = sum5 + getcellvalue6;
            sum6 = sum6 + getcellvalue7;
            sum7 = sum7 + getcellvalue8;
            sum8 = sum8 + getcellvalue9;
            sum9 = sum9 + getcellvalue10;
            sum10 = sum10 + getcellvalue11;
            sum11 = sum11 + getcellvalue12;
            sum12 = sum12 + getcellvalue13;
            sum13 = sum13 + getcellvalue14;
            sum14 = sum14 + getcellvalue15;
            sum15 = sum15 + getcellvalue16;
            sum16 = sum16 + getcellvalue17;
            sum17 = sum17 + getcellvalue18;
            sum18 = sum18 + getcellvalue19;
            sum19 = sum19 + getcellvalue20;
            sum20 = sum20 + getcellvalue21;
            sum21 = sum21 + getcellvalue22;
            sum22 = sum22 + getcellvalue23;
            sum23 = sum23 + getcellvalue24;
            sum24 = sum24 + getcellvalue25;
            sum25 = sum25 + getcellvalue26;
            sum26 = sum26 + getcellvalue27;
            sum27 = sum27 + getcellvalue28;
            sum28 = sum28 + getcellvalue29;
            sum29 = sum29 + getcellvalue30;
            sum30 = sum30 + getcellvalue31;
        }
    }
}

package com.mega.Excel;

/**
 * @author Erinda Febrian - erindafebrian3@gmail.com
 * 13/09/2020
 * class name CombineDTO
 */


public class CombineDTO {

    Integer tlbid;
    Long SUM_OF_AMOUNT_FINALS;
    Integer SUM_OF_QTY;
    Long SUM_OF_FEE_BASED;

    String NAMA_LOKASI;

    public Integer getTlbid() {
        return tlbid;
    }

    public void setTlbid(Integer tlbid) {
        this.tlbid = tlbid;
    }

    public Long getSUM_OF_AMOUNT_FINALS() {
        return SUM_OF_AMOUNT_FINALS;
    }

    public void setSUM_OF_AMOUNT_FINALS(Long SUM_OF_AMOUNT_FINALS) {
        this.SUM_OF_AMOUNT_FINALS = SUM_OF_AMOUNT_FINALS;
    }

    public Integer getSUM_OF_QTY() {
        return SUM_OF_QTY;
    }

    public void setSUM_OF_QTY(Integer SUM_OF_QTY) {
        this.SUM_OF_QTY = SUM_OF_QTY;
    }

    public Long getSUM_OF_FEE_BASED() {
        return SUM_OF_FEE_BASED;
    }

    public void setSUM_OF_FEE_BASED(Long SUM_OF_FEE_BASED) {
        this.SUM_OF_FEE_BASED = SUM_OF_FEE_BASED;
    }

    public String getNAMA_LOKASI() {
        return NAMA_LOKASI;
    }

    public void setNAMA_LOKASI(String NAMA_LOKASI) {
        this.NAMA_LOKASI = NAMA_LOKASI;
    }
    public int compareTo(CombineDTO combineDTO) {
        return (this.getTlbid().compareTo(combineDTO.getTlbid()));
    }
}

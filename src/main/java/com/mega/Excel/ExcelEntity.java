package com.mega.Excel;

/**
 * @author Erinda Febrian - erindafebrian3@gmail.com
 * 12/09/2020
 * class name ExcelEntity
 */


public class ExcelEntity {

    Integer tlbid;
    Long SUM_OF_AMOUNT_FINALS;
    Integer SUM_OF_QTY;
    Long SUM_OF_FEE_BASED;

    public Integer getTlbid() {
        return tlbid;
    }

    public void setTlbid(Integer tlbid) {
        this.tlbid = tlbid;
    }


    public Long getSUM_OF_AMOUNT_FINALS() {
        return SUM_OF_AMOUNT_FINALS;
    }

    public void setSUM_OF_AMOUNT_FINALS(Long SUM_OF_AMOUNT_FINALS) {
        this.SUM_OF_AMOUNT_FINALS = SUM_OF_AMOUNT_FINALS;
    }

    public Integer getSUM_OF_QTY() {
        return SUM_OF_QTY;
    }

    public void setSUM_OF_QTY(Integer SUM_OF_QTY) {
        this.SUM_OF_QTY = SUM_OF_QTY;
    }

    public Long getSUM_OF_FEE_BASED() {
        return SUM_OF_FEE_BASED;
    }

    public void setSUM_OF_FEE_BASED(Long SUM_OF_FEE_BASED) {
        this.SUM_OF_FEE_BASED = SUM_OF_FEE_BASED;
    }

    public int compareTo(ExcelEntity excelEntity) {
        return (this.getTlbid().compareTo(excelEntity.getTlbid()));
    }
}

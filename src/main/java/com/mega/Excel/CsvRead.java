package com.mega.Excel;

import java.io.*;
import java.util.*;

/**
 * @author Erinda Febrian - erindafebrian3@gmail.com
 * 16/09/2020
 * class name CsvRead
 */


public class CsvRead {
    public static void main(String[] args) throws IOException {
        System.out.println("=============================On Process=============================");
        Date dateNow = new Date();
        String csvFile = "RLOC200928.csv";


        List<String> data = new ArrayList<>();

        Object [][] objectArray  = new Object[data.size()][];

        ArrayList<String> datas = null;

        ArrayList<ArrayList<String>> datasUtama = new ArrayList();



        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            String line;

            while ((line = br.readLine()) != null) {

                data = Arrays.asList(line.split("\\s*,\\s*"));
                System.out.println(data);

                System.out.println("Data data 4= " + data.get(4));
                System.out.println("Data data 4 length : "+data.get(4).length());
                System.out.println("Data 12 : " +data.get(12));
                System.out.println("Data amount : " + data.get(1));

                datas = new ArrayList();

                datas.add(data.get(2));
                datas.add(data.get(8));
                datas.add(data.get(5));
                datas.add(data.get(12));
                datas.add(data.get(1));

                datasUtama.add(datas);



            }

//            datasUtama.add(datas);

//            System.out.println(records);
            System.out.println("TEST "+datas);
            System.out.println("TEST DAU"+datasUtama);

//            System.out.println("TEST F "+datasf);
//            System.out.println("TEST DAUF"+datasUtamaf);


        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(datas.size());

        System.out.println(datasUtama.size());

        System.out.println("======================Create File=================================");

        Date newDate = new Date();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(newDate);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DATE);
        int hour = calendar.get(Calendar.HOUR);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);
        File directoryPath = new File("E:/excel").getAbsoluteFile();

        String enter = System.getProperty("line.separator");

        FileWriter csvWriter = null;

        if ((day>=1 && day <= 9) && (month>=1 && month <=8)){
            csvWriter = new FileWriter(directoryPath + "//CCLOCNTF_"+year+0+(month+1)+0+day+".txt", true);
            csvWriter.write("HCCLOCNTF  "+year+0+(month+1)+0+day);
            csvWriter.write(enter);
        } else if ((day>=1 && day <= 9) && (month >= 9)){
            csvWriter = new FileWriter(directoryPath + "//CCLOCNTF_"+year+(month+1)+0+day+".txt", true);
            csvWriter.write("HCCLOCNTF  "+year+(month+1)+0+day);
            csvWriter.write(enter);
        } else if ((day>=10) && (month>=1 && month <=8)){
            csvWriter = new FileWriter(directoryPath + "//CCLOCNTF_"+year+0+(month+1)+day+".txt", true);
            csvWriter.write("HCCLOCNTF  "+year+0+(month+1)+day);
            csvWriter.write(enter);
        }
        else {
            csvWriter = new FileWriter(directoryPath + "//CCLOCNTF_"+year+(month+1)+day+".txt", true);
            csvWriter.write("HCCLOCNTF  "+year+(month+1)+day);
            csvWriter.write(enter);
        }

        int sizeDatas = datas.size();
        int sizeDau = datasUtama.size();

        System.out.println(sizeDatas);
        System.out.println(sizeDau);


        System.out.println(datas);
        System.out.println(datasUtama);


        ArrayList data1 = new ArrayList();
        ArrayList data2 = new ArrayList();

        for (int i = 0; i < sizeDau; i++) {

            data1 = datasUtama.get(i);

            System.out.println("Data satu : " + data1);


            if (i==1 || i>=1){

                System.out.println("datas : "+data1.get(4));

                String noRek = data1.get(1).toString();
                String res = noRek.substring(Math.max(0, noRek.length() - 19));
                Integer sizeRes = res.length();

                String noHp = data1.get(2).toString();
                Integer sizeNoHp= noHp.length();

                String amount = data1.get(4).toString();
                amount = amount.replace(".","");
                Integer sizeAmount = amount.length();

                System.out.println("amount : "+amount);
                System.out.println("size amount : " + sizeAmount);


               if (data1.get(3).equals("S")){

                   System.out.println("data S : " +data1.get(1).toString());

                   System.out.println("norek : "+noRek);
                   System.out.println(noRek.length());
                   System.out.println("res : "+res);



                   System.out.println("data 1 ke 0"+data1.get(0));
                    System.out.println(data1.get(1));
                    csvWriter.write("DCCLOC02" + data1.get(0));
                    csvWriter.append("   ");
                   if (sizeNoHp==9){
                       csvWriter.write("" + noHp);
                       csvWriter.append("      ");
                   } else if (sizeNoHp==10){
                       csvWriter.write("" + noHp);
                       csvWriter.append("     ");
                   } else if (sizeNoHp==11){
                       csvWriter.write("" + noHp);
                       csvWriter.append("    ");
                   } else  if (sizeNoHp==12){
                       csvWriter.write("" + noHp);
                       csvWriter.append("   ");
                   } else if (sizeNoHp==13){
                       csvWriter.write("" + noHp);
                       csvWriter.append("  ");
                   } else {
                       csvWriter.write("" + noHp);
                       csvWriter.append(" ");
                   }

                   if (sizeRes==1){
                       csvWriter.write("000000000000000000"+res);
                   }
                   else if (sizeRes==2){
                       csvWriter.write("00000000000000000"+res);
                   } else if (sizeRes==3){
                       csvWriter.write("0000000000000000"+res);
                   }else  if (sizeRes==4){
                       csvWriter.write("000000000000000"+res);
                   }else if (sizeRes==5){
                       csvWriter.write("00000000000000"+res);
                   }else if (sizeRes==6){
                       csvWriter.write("0000000000000"+res);
                   }else if (sizeRes==7){
                       csvWriter.write("000000000000"+res);
                   }else  if (sizeRes==8){
                       csvWriter.write("00000000000"+res);
                   }else  if (sizeRes==9){
                       csvWriter.write("0000000000"+res);
                   }else if (sizeRes==10){
                       csvWriter.write("000000000"+res);
                   }else if (sizeRes==11){
                       csvWriter.write("00000000"+res);
                   }else if (sizeRes==12){
                       csvWriter.write("0000000"+res);
                   }else if (sizeRes==13){
                       csvWriter.write("000000"+res);
                   }else if (sizeRes==14){
                       csvWriter.write("00000"+res);
                   }else if (sizeRes==15){
                       csvWriter.write("0000"+res);
                   }else if (sizeRes==16){
                       csvWriter.write("000"+res);
                   }else if (sizeRes==17){
                       csvWriter.write("00"+res);
                   }else if (sizeRes==18){
                       csvWriter.write("0"+res);
                   }else if (sizeRes>=19){
                       csvWriter.write(""+res);
                   }



                   if (sizeAmount==5){
                        csvWriter.write("00000000000"+amount+".");
                    }else if (sizeAmount==6){
                        csvWriter.write("000000000"+amount+".");
                    }else if (sizeAmount==7){
                        csvWriter.write("00000000"+amount+".");
                    }else if (sizeAmount==8){
                        csvWriter.write("0000000"+amount+".");
                    }else if (sizeAmount==9){
                        csvWriter.write("000000"+amount+".");
                    }else if (sizeAmount==10){
                        csvWriter.write("00000"+amount+".");
                    }else if (sizeAmount==11){
                        csvWriter.write("0000"+amount+".");
                    }else if (sizeAmount==12){
                        csvWriter.write("000"+amount+".");
                    } else if (sizeAmount==13){
                        csvWriter.write("00"+amount+".");
                    }else if (sizeAmount==14){
                        csvWriter.write("0"+amount+".");
                    } else  {
                        csvWriter.write(amount+".");
                    }
                    csvWriter.write(enter);
               } else if (data1.get(3).equals("F")){
                   System.out.println("data F : " +data1.get(1).toString());
                   System.out.println("data 1 ke 0"+data1.get(0));
                   System.out.println(data1.get(1));
                   csvWriter.write("DCCLOC03" + data1.get(0));
                   csvWriter.append("   ");
                   if (sizeNoHp==9){
                       csvWriter.write("" + noHp);
                       csvWriter.append("      ");
                   } else if (sizeNoHp==10){
                       csvWriter.write("" + noHp);
                       csvWriter.append("     ");

                   } else if (sizeNoHp==11){
                       csvWriter.write("" + noHp);
                       csvWriter.append("    ");
                   } else  if (sizeNoHp==12){
                       csvWriter.write("" + noHp);
                       csvWriter.append("   ");
                   } else if (sizeNoHp==13){
                       csvWriter.write("" + noHp);
                       csvWriter.append("  ");
                   } else {
                       csvWriter.write("" + noHp);
                       csvWriter.append(" ");
                   }

                   if (sizeRes==1){
                       csvWriter.write("000000000000000000"+res);
                   }
                   else if (sizeRes==2){
                       csvWriter.write("00000000000000000"+res);
                   } else if (sizeRes==3){
                       csvWriter.write("0000000000000000"+res);
                   }else  if (sizeRes==4){
                       csvWriter.write("000000000000000"+res);
                   }else if (sizeRes==5){
                       csvWriter.write("00000000000000"+res);
                   }else if (sizeRes==6){
                       csvWriter.write("0000000000000"+res);
                   }else if (sizeRes==7){
                       csvWriter.write("000000000000"+res);
                   }else  if (sizeRes==8){
                       csvWriter.write("00000000000"+res);
                   }else  if (sizeRes==9){
                       csvWriter.write("0000000000"+res);
                   }else if (sizeRes==10){
                       csvWriter.write("000000000"+res);
                   }else if (sizeRes==11){
                       csvWriter.write("00000000"+res);
                   }else if (sizeRes==12){
                       csvWriter.write("0000000"+res);
                   }else if (sizeRes==13){
                       csvWriter.write("000000"+res);
                   }else if (sizeRes==14){
                       csvWriter.write("00000"+res);
                   }else if (sizeRes==15){
                       csvWriter.write("0000"+res);
                   }else if (sizeRes==16){
                       csvWriter.write("000"+res);
                   }else if (sizeRes==17){
                       csvWriter.write("00"+res);
                   }else if (sizeRes==18){
                       csvWriter.write("0"+res);
                   }else if (sizeRes>=19){
                       csvWriter.write(""+res);
                   }

                   if (sizeAmount==5){
                       csvWriter.write("00000000000"+amount+".");
                   }else if (sizeAmount==6){
                       csvWriter.write("000000000"+amount+".");
                   }else if (sizeAmount==7){
                       csvWriter.write("00000000"+amount+".");
                   }else if (sizeAmount==8){
                       csvWriter.write("0000000"+amount+".");
                   }else if (sizeAmount==9){
                       csvWriter.write("000000"+amount+".");
                   }else if (sizeAmount==10){
                       csvWriter.write("00000"+amount+".");
                   }else if (sizeAmount==11){
                       csvWriter.write("0000"+amount+".");
                   }else if (sizeAmount==12){
                       csvWriter.write("000"+amount+".");
                   } else if (sizeAmount==13){
                       csvWriter.write("00"+amount+".");
                   }else if (sizeAmount==14){
                       csvWriter.write("0"+amount+".");
                   } else  {
                       csvWriter.write(amount+".");
                   }
                   csvWriter.write(enter);

               } else {
                   System.out.println("data undefined");

               }
            }
        }

        System.out.println("data 1 : " +data1);
        System.out.println("tot data 1 : "+(sizeDau));

         csvWriter.write("T0000000000"+(sizeDau-1));
        csvWriter.flush();
        csvWriter.close();


        System.out.println("File have been created");
    }

}

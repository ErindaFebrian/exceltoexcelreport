package com.mega.Excel.Repo;

import com.mega.Excel.Dto.DataDto;
import com.mega.Excel.Model.DataEntity;
import org.eclipse.persistence.jpa.jpql.parser.DateTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * @author Erinda Febrian - erindafebrian3@gmail.com
 * 29/09/2020
 * class name DataRepository
 */

@Repository
public interface DataRepository extends JpaRepository<DataEntity,Integer> {
    @Query(value = "select * from tm_msmile_api " +
            "where DATE(create_date) = ?1 " +
            "and type_case = ?2 " +
            "AND flag_rpa = ?3 ",nativeQuery = true)
    List<DataDto> getData(Date create_date, String type_case, String flag_rpa);

}

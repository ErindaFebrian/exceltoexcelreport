package com.mega.Excel.Controller;

import com.mega.Excel.Dto.ApiResponse;
import com.mega.Excel.Dto.DataDto;
import com.mega.Excel.Repo.DataRepository;
import org.eclipse.persistence.jpa.jpql.parser.DateTime;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileWriter;
import java.util.*;
import java.util.logging.Logger;

/**
 * @author Erinda Febrian - erindafebrian3@gmail.com
 * 30/09/2020
 * class name DataController
 */


@RestController
@RequestMapping({"/api"})
public class DataController {

//    Logger log = LoggerFactory.getLogger(DataController.class);

    @Autowired
    DataRepository dataRepository;

    @GetMapping
    public ResponseEntity<?> getData (@RequestParam Date create_date, String type_case, String flag_rpa){
        ResponseEntity<ApiResponse> result = null;
        try{
            List<DataDto> listdata = null;

           listdata=dataRepository.getData(create_date, type_case, flag_rpa);

           result = ResponseEntity.ok(new ApiResponse(listdata));

//            Map <String, Object> data = new HashMap<>();
//            data.put("applications", listdata);

//            byte [] bytedata = null;
//
//            System.out.println("======================Create File=================================");
//
//            Date newDate = new Date();
//            Calendar calendar = new GregorianCalendar();
//            calendar.setTime(newDate);
//            int year = calendar.get(Calendar.YEAR);
//            int month = calendar.get(Calendar.MONTH);
//            int day = calendar.get(Calendar.DATE);
//            int hour = calendar.get(Calendar.HOUR);
//            int minute = calendar.get(Calendar.MINUTE);
//            int second = calendar.get(Calendar.SECOND);
//            File directoryPath = new File("E:/excel").getAbsoluteFile();
//
//            String enter = System.getProperty("line.separator");
//
//            FileWriter csvWriter = new FileWriter(directoryPath + "//CCLOCNTF_"+year+0+(month+1)+day+".txt", true);
//            csvWriter.write("HCCLOCNTF   "+year+0+(month+1)+day);
//            csvWriter.write(enter);


//            return ResponseEntity.ok(new ApiRes(true,
//                    "Get data success", response));

        }catch (Exception e){
            e.printStackTrace();
//            log.error("Exception", e);
            return ResponseEntity.ok(new ApiResponse(false,
                    "Get data failed", new String[] { e.getMessage() }));
        }
        return result;
    }

}

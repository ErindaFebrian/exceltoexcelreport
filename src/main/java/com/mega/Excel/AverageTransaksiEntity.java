package com.mega.Excel;

/**
 * @author Erinda Febrian - erindafebrian3@gmail.com
 * 13/09/2020
 * class name AverageTransaksiEntity
 */


public class AverageTransaksiEntity {
    Integer tlbid;
    String NAMA_LOKASI;

    public Integer getTlbid() {
        return tlbid;
    }

    public void setTlbid(Integer tlbid) {
        this.tlbid = tlbid;
    }

    public String getNAMA_LOKASI() {
        return NAMA_LOKASI;
    }

    public void setNAMA_LOKASI(String NAMA_LOKASI) {
        this.NAMA_LOKASI = NAMA_LOKASI;
    }

    public int compareTo2(AverageTransaksiEntity averageTransaksiEntity) {
        return (this.getTlbid().compareTo(averageTransaksiEntity.getTlbid()));
    }
}

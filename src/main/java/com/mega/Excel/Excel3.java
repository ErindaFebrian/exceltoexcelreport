package com.mega.Excel;

import java.io.*;

import java.util.*;

//import com.monitorjbl.xlsx.StreamingReader;
//import com.monitorjbl.xlsx.impl.StreamingSheetReader;
import com.monitorjbl.xlsx.StreamingReader;
import com.monitorjbl.xlsx.impl.StreamingSheet;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


/**
 * @author Erinda Febrian - erindafebrian3@gmail.com
 * 06/09/2020
 * class name Excel3
 */


public class Excel3 {
    public static void main(String[] args) throws  IOException {
        try {
            System.out.println("=============================On Process=============================");
            Date dateNow = new Date();
            String buffer = "";

            Integer count = 0;
            Integer countBuffer = 0;
            Long SUM_OF_AMOUNT_FINALS = Long.parseLong("0");
            Integer SUM_OF_QTY = 0;
            Long SUM_OF_FEE_BASED = Long.parseLong("0");
            ArrayList<ExcelEntity> listExcelEntity = new ArrayList<ExcelEntity>();


            System.out.println("Read File 1");
            File directoryPath = new File("").getAbsoluteFile();
//            InputStream is = new FileInputStream(new File(directoryPath + "\\coba_file1.xlsx"));
//            InputStream is = new FileInputStream(new File(directoryPath + "\\CTOP - Data Transaksi & Fee Based ATM_Mei_2019_v1.0_RESUME-1.xlsx"));
            InputStream is = new FileInputStream(new File(directoryPath + "\\test.xlsx"));

            StreamingReader reader = StreamingReader.builder().rowCacheSize(100)
                    .bufferSize(4096)
                    .sheetIndex(0)
                    .read(is);
//            Sheet sheet = reader.g

            File file = new File(directoryPath + "\\test.xlsx");
            Workbook workbook = StreamingReader.builder()
                            .rowCacheSize(100)
                            .bufferSize(4096)
                            .open(file);
            Sheet sheet = workbook.getSheetAt(0);

            int lastRowNum = sheet.getLastRowNum()+1;


            for (Row r : reader) {
                int testCount = count+1;
                for (Cell c : r) {
                    if (c.getRowIndex() == 0 || c.getRowIndex() == 1) {
                        continue;
                    }

                    if (c.getColumnIndex() == 1) {
                        //Menentukan Nilai Awal Buffer


                        if (buffer.equals("") && c.getColumnIndex() == 1) {
                            buffer = String.valueOf(c.getStringCellValue());
                        } else if (c.getStringCellValue().equalsIgnoreCase(buffer)) {
                            SUM_OF_AMOUNT_FINALS += SUM_AMOUNT_FINALS_CONDITION(r);
                            SUM_OF_QTY += SUM_OF_FINAL_QTY(r);
                            SUM_OF_FEE_BASED += SUM_OF_FINAL_FEE_BASED(r);

                        } else if (!c.getStringCellValue().equalsIgnoreCase(buffer)) {
                            ExcelEntity excelEntity = new ExcelEntity();
                            excelEntity.setTlbid(Integer.parseInt(buffer));
                            excelEntity.setSUM_OF_AMOUNT_FINALS(SUM_OF_AMOUNT_FINALS);
                            excelEntity.setSUM_OF_QTY(SUM_OF_QTY);
                            excelEntity.setSUM_OF_FEE_BASED(SUM_OF_FEE_BASED);
                            excelEntity.compareTo(excelEntity);
                            listExcelEntity.add(excelEntity);

                            buffer = String.valueOf(c.getStringCellValue());

                            SUM_OF_AMOUNT_FINALS = Long.parseLong("0");
                            SUM_OF_QTY = Integer.parseInt("0");
                            SUM_OF_FEE_BASED = Long.parseLong("0");
                            countBuffer++;

                        }
                        else if (testCount == lastRowNum) {
                            System.out.println("row terakhir");

//                            ExcelEntity excelEntity = new ExcelEntity();
//                            excelEntity.setTlbid(Integer.parseInt(buffer));
//                            excelEntity.setSUM_OF_AMOUNT_FINALS(SUM_OF_AMOUNT_FINALS);
//                            excelEntity.setSUM_OF_QTY(SUM_OF_QTY);
//                            excelEntity.setSUM_OF_FEE_BASED(SUM_OF_FEE_BASED);
//                            excelEntity.compareTo(excelEntity);
//                            listExcelEntity.add(excelEntity);
//
//                            buffer = String.valueOf(c.getStringCellValue());
//
//                            SUM_OF_AMOUNT_FINALS = Long.parseLong("0");
//                            SUM_OF_QTY = Integer.parseInt("0");
//                            SUM_OF_FEE_BASED = Long.parseLong("0");
//                            countBuffer++;
                        }
                        else {
                            throw new Exception("Data Error !");
                        }
                    }
                }
                count++;
            }

            listExcelEntity.sort(Comparator.comparing(ExcelEntity::getTlbid));
            ArrayList<ExcelEntity> finalListExcel = new ArrayList<ExcelEntity>();

            buffer = "6";
            SUM_OF_AMOUNT_FINALS = Long.parseLong("0");
            SUM_OF_QTY = Integer.parseInt("0");
            SUM_OF_FEE_BASED = Long.parseLong("0");
            Integer i = 1;
            for (ExcelEntity excelEntity : listExcelEntity) {
                if (Long.parseLong(buffer) == excelEntity.getTlbid()) {
                    System.out.println("Tlid : " + excelEntity.getTlbid());
                    System.out.println("buffer : " + buffer);
                    SUM_OF_AMOUNT_FINALS += excelEntity.getSUM_OF_AMOUNT_FINALS();
                    SUM_OF_QTY += excelEntity.getSUM_OF_QTY();
                    SUM_OF_FEE_BASED += excelEntity.getSUM_OF_FEE_BASED();
                } else {
                    ExcelEntity finalExcelEntity = new ExcelEntity();
                    finalExcelEntity.setTlbid(Integer.parseInt(buffer));
                    finalExcelEntity.setSUM_OF_AMOUNT_FINALS(SUM_OF_AMOUNT_FINALS);
                    finalExcelEntity.setSUM_OF_QTY(SUM_OF_QTY);
                    finalExcelEntity.setSUM_OF_FEE_BASED(SUM_OF_FEE_BASED);
                    finalListExcel.add(finalExcelEntity);

                    buffer = excelEntity.getTlbid().toString();

//                    SUM_OF_AMOUNT_FINALS = Long.parseLong("0");
//                    SUM_OF_QTY = Integer.parseInt("0");
//                    SUM_OF_FEE_BASED = Long.parseLong("0");
//                    countBuffer++;
                }
                i++;
            }

//            for (int j = 0; j < finalListExcel.size() ; j++) {
////
//                System.out.println("data file tlbid: " + finalListExcel.get(j).getTlbid());
////
//            }


            for (ExcelEntity excelEntity : finalListExcel) {
                System.out.println("TLBID: " + excelEntity.getTlbid() + ", " + "Total SUM_OF_AMOUNT_FINALS: "
                        + excelEntity.getSUM_OF_AMOUNT_FINALS() + ", " +
                        "Total SUM_OF_QTY: " + excelEntity.getSUM_OF_QTY() + ", "
                        + "Total SUM_OF_FEE_BASE: " + excelEntity.getSUM_OF_FEE_BASED());
            }
            System.out.println("From Time: " + dateNow);
            System.out.println("Current Time: " + new Date());
            System.out.println("Total Count Buffer: " + countBuffer);
            System.out.println("Total Count Row: " + count);


            System.out.println("=============================On Process 2=============================");
            Date dateNow2 = new Date();
            String buffer2 = "6";
            Integer count2 = 0;
            Integer countBuffer2 = 0;
            String SET_NAMA_LOKASI = "KIDS CITY TRANSMART SETIABUDI SEMARANG";
            ArrayList<AverageTransaksiEntity> listAverageTransaksiEntity = new ArrayList<AverageTransaksiEntity>();


            System.out.println("Read File 2");
            File directoryPath2 = new File("").getAbsoluteFile();
//            InputStream is2 = new FileInputStream(new File(directoryPath + "\\coba_file2.xlsx"));
            InputStream is2 = new FileInputStream(new File(directoryPath2 + "\\Average Transaksi ATM per TID.xlsx"));
            StreamingReader reader2 = StreamingReader.builder()
                    .rowCacheSize(100)
                    .bufferSize(4096)
                    .sheetIndex(0)
                    .read(is2);

            for (Row r2 : reader2) {
                for (Cell c2 : r2) {
                    if (c2.getRowIndex() == 0 || c2.getRowIndex() == 1) {
                        continue;
                    }
                    if (c2.getColumnIndex() == 1) {
                        if (c2.getStringCellValue().equalsIgnoreCase(buffer2) && c2.getColumnIndex() == 1) {
                            SET_NAMA_LOKASI = r2.getCell(2).getStringCellValue();

                        } else if (!c2.getStringCellValue().equalsIgnoreCase(buffer2) && c2.getColumnIndex() == 1) {
                            AverageTransaksiEntity averageTransaksiEntity = new AverageTransaksiEntity();
                            averageTransaksiEntity.setTlbid(Integer.parseInt(buffer2));
                            averageTransaksiEntity.setNAMA_LOKASI(SET_NAMA_LOKASI);
                            averageTransaksiEntity.compareTo2(averageTransaksiEntity);
                            listAverageTransaksiEntity.add(averageTransaksiEntity);
                            buffer2 = c2.getStringCellValue();
                            SET_NAMA_LOKASI = r2.getCell(2).getStringCellValue();
                            countBuffer2++;
                        }
                    }
                }
                count2++;
            }

            listAverageTransaksiEntity.sort(Comparator.comparing(AverageTransaksiEntity::getTlbid));
            ArrayList<AverageTransaksiEntity> finalListExcel2 = new ArrayList<AverageTransaksiEntity>();
            buffer2 = "6";
            SET_NAMA_LOKASI = "KIDS CITY TRANSMART SETIABUDI SEMARANG";
            Integer i2 = 1;
            for (AverageTransaksiEntity averageTransaksiEntity : listAverageTransaksiEntity) {
                if (Long.parseLong(buffer2) == averageTransaksiEntity.getTlbid()) {
                    SET_NAMA_LOKASI = averageTransaksiEntity.getNAMA_LOKASI();
                } else {
                    AverageTransaksiEntity finalAverageTransakasiEntity = new AverageTransaksiEntity();
                    finalAverageTransakasiEntity.setTlbid(Integer.parseInt(buffer2));
                    finalAverageTransakasiEntity.setNAMA_LOKASI(SET_NAMA_LOKASI);
                    finalListExcel2.add(finalAverageTransakasiEntity);
                    buffer2 = averageTransaksiEntity.getTlbid().toString();
                    SET_NAMA_LOKASI = averageTransaksiEntity.getNAMA_LOKASI();
                }
                i2++;
            }


            System.out.println("data file 2 : "+finalListExcel2);
            for (AverageTransaksiEntity averageTransaksiEntity : finalListExcel2) {
                System.out.println("TLBID: " + averageTransaksiEntity.getTlbid() + ", " + "Nama Lokasi: "
                        + averageTransaksiEntity.getNAMA_LOKASI());
            }
            System.out.println("From Time: " + dateNow2);
            System.out.println("Current Time: " + new Date());
            System.out.println("Total Count Buffer2: " + countBuffer2);
            System.out.println("Total Count Row: " + count2);


            System.out.println("=============================On Process Menyimpan Data=============================");
            System.out.println("Meyimpan Data...");
            String filename = "ExcelFileCombine.xlsx";
            Date dateNow3 = new Date();
            FileOutputStream fos = new FileOutputStream(filename);
            XSSFWorkbook workbooks = new XSSFWorkbook();
            XSSFSheet sheets = workbooks.createSheet("Data Rekap");

            String[] dataHeader = {"No", "tlbid","Description","Sum of amount_finals","Sum of Qty","Sum of fee_based"};

            Row row = sheets.createRow(0);
            for (int header = 0; header <= dataHeader.length-1; header++)
            {

                Cell cell = row.createCell(header);
                cell.setCellValue(dataHeader[header]);

            }

            int no = 1;

            List<Integer> tlbidData1 = new ArrayList<>();
            for (int n = 0; n < finalListExcel.size() ; n++) {
                tlbidData1.add(finalListExcel.get(n).getTlbid());
            }

            System.out.println("tlbidData1 = " +tlbidData1.size());
            Map<String, Object> mapDataTrue = new HashMap<>();

            List <Object> dataList = new ArrayList<>();
            Boolean data = null;

            Integer tlbidDataId = 0;
            String tlbidDataLokasi = "";

            for (int m = 0; m < finalListExcel2.size() ; m++) {
                tlbidDataId= finalListExcel2.get(m).getTlbid();
                tlbidDataLokasi= finalListExcel2.get(m).getNAMA_LOKASI();
                data = tlbidData1.contains(tlbidDataId);

//                String dataCollect = tlbidDataId+"|"+tlbidDataLokasi;
//                String[] dataSplit = dataCollect.split("|");
//                String part1 = dataSplit[0];
//                String part2 =dataSplit[1];

                if (data.booleanValue()){
                    dataList.add(tlbidDataLokasi);
                }

            }

            mapDataTrue.put("dataList", dataList);
            System.out.println("data true result  : " +mapDataTrue.get("dataList"));
            System.out.println(dataList.size());

            List <String> dataLokasi = (List<String>) mapDataTrue.get("datalist");

//            System.out.println(tlbidData2)
//            System.out.println("cek data array = " +tlbidData2.size());
//            System.out.println("cek data array = " +descriptionData2.size());
//            System.out.println("cek data tlbidData1 = "+tlbidData1);

            for (int datas = 0; datas <= dataList.size()-1; datas++) {
                Row rowDatas = sheets.createRow(datas+1);

//                tlbidDataUtama.add(finalListExcel.get(datas).getTlbid());

                    for (int k = 0; k <= dataHeader.length; k++) {
                        Cell cellData = rowDatas.createCell(k);
//                        Cell cellData2 = rowDatas2.createCell(k);
                        if (k == 0) {
                            cellData.setCellValue(no);
                            no++;
                        } else if (k == 1) {
                            cellData.setCellValue(finalListExcel.get(datas).getTlbid());
                        }
                        else if(k==2) {
                               cellData.setCellValue(dataList.get(datas).toString());
                        }
                        else if (k == 3) {
                            cellData.setCellValue(finalListExcel.get(datas).getSUM_OF_AMOUNT_FINALS());
                        } else if (k == 4) {
                            cellData.setCellValue(finalListExcel.get(datas).getSUM_OF_QTY());
                        } else if (k == 5) {
                            cellData.setCellValue(finalListExcel.get(datas).getSUM_OF_FEE_BASED());
                        }
                    }

            }



            workbooks.write(fos);
            fos.flush();
            fos.close();
            System.out.println("Data sudah tersimpan di file excel");
            System.out.println("From Time: " + dateNow3);
            System.out.println("Current Time: " + new Date());

        }catch(Exception e){
            System.out.println(e);
            System.out.println(e.getMessage());
        }
    }



    static Long SUM_AMOUNT_FINALS_CONDITION(Row r){
        if(r.getCell(4).getStringCellValue().equalsIgnoreCase("") ||
                r.getCell(4).getStringCellValue().equalsIgnoreCase("- 0") ) {

        }else {
            return currenctyToLong(r.getCell(4).getStringCellValue());
        }

        if(r.getCell(7).getStringCellValue().equalsIgnoreCase("") ||
                r.getCell(7).getStringCellValue().equalsIgnoreCase("- 0") ) {

        } else {
            return currenctyToLong(r.getCell(7).getStringCellValue());
        }

        if(r.getCell(10).getStringCellValue().equalsIgnoreCase("") ||
                r.getCell(10).getStringCellValue().equalsIgnoreCase("- 0") ) {

        }else{
            return currenctyToLong(r.getCell(10).getStringCellValue());
        }

        if(r.getCell(13).getStringCellValue().equalsIgnoreCase("") ||
                r.getCell(13).getStringCellValue().equalsIgnoreCase("- 0") ) {
        }else{
            return currenctyToLong(r.getCell(13).getStringCellValue());
        }

        if(r.getCell(16).getStringCellValue().equalsIgnoreCase("") ||
                r.getCell(16).getStringCellValue().equalsIgnoreCase("- 0") ) {
        }else{
            return currenctyToLong(r.getCell(16).getStringCellValue());
        }

        if(r.getCell(19).getStringCellValue().equalsIgnoreCase("") ||
                r.getCell(19).getStringCellValue().equalsIgnoreCase("- 0") ) {
        }else{
            return currenctyToLong(r.getCell(19).getStringCellValue());
        }

        if(r.getCell(22).getStringCellValue().equalsIgnoreCase("") ||
                r.getCell(22).getStringCellValue().equalsIgnoreCase("- 0") ) {
        }else{
            return currenctyToLong(r.getCell(22).getStringCellValue());
        }

        if(r.getCell(25).getStringCellValue().equalsIgnoreCase("") ||
                r.getCell(25).getStringCellValue().equalsIgnoreCase("- 0") ) {
        }else{
            return currenctyToLong(r.getCell(25).getStringCellValue());
        }

        if(r.getCell(28).getStringCellValue().equalsIgnoreCase("") ||
                r.getCell(28).getStringCellValue().equalsIgnoreCase("- 0") ) {
        }else{
            return currenctyToLong(r.getCell(28).getStringCellValue());
        }

        if(r.getCell(31).getStringCellValue().equalsIgnoreCase("") ||
                r.getCell(31).getStringCellValue().equalsIgnoreCase("- 0") ) {
        }else{
            return currenctyToLong(r.getCell(31).getStringCellValue());
        }

        return Long.parseLong("0");
    }

    static Integer SUM_OF_FINAL_QTY(Row r){
        if(r.getCell(5).getStringCellValue().equalsIgnoreCase("") ||
                r.getCell(5).getStringCellValue().equalsIgnoreCase("- 0") ) {

        }else {
            return Integer.parseInt(r.getCell(5).getStringCellValue());
        }

        if(r.getCell(8).getStringCellValue().equalsIgnoreCase("") ||
                r.getCell(8).getStringCellValue().equalsIgnoreCase("- 0") ) {

        } else {
            return Integer.parseInt(r.getCell(8).getStringCellValue());
        }

        if(r.getCell(11).getStringCellValue().equalsIgnoreCase("") ||
                r.getCell(11).getStringCellValue().equalsIgnoreCase("- 0") ) {

        }else{
            return Integer.parseInt(r.getCell(11).getStringCellValue());
        }

        if(r.getCell(14).getStringCellValue().equalsIgnoreCase("") ||
                r.getCell(14).getStringCellValue().equalsIgnoreCase("- 0") ) {
        }else{
            return Integer.parseInt(r.getCell(14).getStringCellValue());
        }

        if(r.getCell(17).getStringCellValue().equalsIgnoreCase("") ||
                r.getCell(17).getStringCellValue().equalsIgnoreCase("- 0") ) {
        }else{
            return Integer.parseInt(r.getCell(17).getStringCellValue());
        }

        if(r.getCell(20).getStringCellValue().equalsIgnoreCase("") ||
                r.getCell(20).getStringCellValue().equalsIgnoreCase("- 0") ) {
        }else{
            return Integer.parseInt(r.getCell(20).getStringCellValue());
        }

        if(r.getCell(23).getStringCellValue().equalsIgnoreCase("") ||
                r.getCell(23).getStringCellValue().equalsIgnoreCase("- 0") ) {
        }else{
            return Integer.parseInt(r.getCell(23).getStringCellValue());
        }

        if(r.getCell(26).getStringCellValue().equalsIgnoreCase("") ||
                r.getCell(26).getStringCellValue().equalsIgnoreCase("- 0") ) {
        }else{
            return Integer.parseInt(r.getCell(26).getStringCellValue());
        }

        if(r.getCell(29).getStringCellValue().equalsIgnoreCase("") ||
                r.getCell(29).getStringCellValue().equalsIgnoreCase("- 0") ) {
        }else{
            return Integer.parseInt(r.getCell(29).getStringCellValue());
        }

        if(r.getCell(32).getStringCellValue().equalsIgnoreCase("") ||
                r.getCell(32).getStringCellValue().equalsIgnoreCase("- 0") ) {
        }else{
            return Integer.parseInt(r.getCell(32).getStringCellValue());
        }

        return 0;
    }

    static Long SUM_OF_FINAL_FEE_BASED(Row r){
        if(r.getCell(6).getStringCellValue().equalsIgnoreCase("") ||
                r.getCell(6).getStringCellValue().equalsIgnoreCase("- 0") ) {

        }else {
            return currenctySumToLong(r.getCell(6).getStringCellValue());
        }

        if(r.getCell(9).getStringCellValue().equalsIgnoreCase("") ||
                r.getCell(9).getStringCellValue().equalsIgnoreCase("- 0") ) {

        } else {
            return currenctySumToLong(r.getCell(9).getStringCellValue());
        }

        if(r.getCell(12).getStringCellValue().equalsIgnoreCase("") ||
                r.getCell(12).getStringCellValue().equalsIgnoreCase("- 0") ) {

        }else{
            return currenctySumToLong(r.getCell(12).getStringCellValue());
        }

        if(r.getCell(15).getStringCellValue().equalsIgnoreCase("") ||
                r.getCell(15).getStringCellValue().equalsIgnoreCase("- 0") ) {
        }else{
            return currenctySumToLong(r.getCell(15).getStringCellValue());
        }

        if(r.getCell(18).getStringCellValue().equalsIgnoreCase("") ||
                r.getCell(18).getStringCellValue().equalsIgnoreCase("- 0") ) {
        }else{
            return currenctySumToLong(r.getCell(18).getStringCellValue());
        }

        if(r.getCell(21).getStringCellValue().equalsIgnoreCase("") ||
                r.getCell(21).getStringCellValue().equalsIgnoreCase("- 0") ) {
        }else{
            return currenctySumToLong(r.getCell(21).getStringCellValue());
        }

        if(r.getCell(24).getStringCellValue().equalsIgnoreCase("") ||
                r.getCell(24).getStringCellValue().equalsIgnoreCase("- 0") ) {
        }else{
            return currenctySumToLong(r.getCell(24).getStringCellValue());
        }

        if(r.getCell(27).getStringCellValue().equalsIgnoreCase("") ||
                r.getCell(27).getStringCellValue().equalsIgnoreCase("- 0") ) {
        }else{
            return currenctySumToLong(r.getCell(27).getStringCellValue());
        }

        if(r.getCell(30).getStringCellValue().equalsIgnoreCase("") ||
                r.getCell(30).getStringCellValue().equalsIgnoreCase("- 0") ) {
        }else{
            return currenctySumToLong(r.getCell(30).getStringCellValue());
        }

        if(r.getCell(33).getStringCellValue().equalsIgnoreCase("") ||
                r.getCell(33).getStringCellValue().equalsIgnoreCase("- 0") ) {
        }else{
            return currenctySumToLong(r.getCell(33).getStringCellValue());
        }

        return Long.parseLong("0");
    }

//    static String NAMA_LOKASI_CONDITION(Row r2){
//
//        if(r2.getCell(2).getStringCellValue().equalsIgnoreCase("") ||
//                r2.getCell(2).getStringCellValue().equalsIgnoreCase("- 0") ) {
//        }else{
//            return r2.getCell(2).getStringCellValue();
//        }
//        return null;
//    }

    static Long currenctyToLong(String currency){
        String replaceDot = "";
        String replaceComma = currency.replace(",", "");
        if(String.valueOf(currency.charAt(currency.length() - 3)).equalsIgnoreCase(".")){
            replaceDot = replaceComma.replace(".", "");
            String removeLast2Digit = replaceDot.substring(0, replaceDot.length() - 2);
            return Long.parseLong(removeLast2Digit);
        }else{
            replaceDot = replaceComma.replace(".", "");
            return Long.parseLong(replaceDot);
        }
    }

    static Long currenctySumToLong(String currency){
        String replaceDot = "";
        String replaceComma = currency.replace(",", "");
        if(String.valueOf(currency.charAt(currency.length() - 3)).equalsIgnoreCase(".")){
            replaceDot = replaceComma.replace(".", "");
            String removeLast2Digit = replaceDot.substring(0, replaceDot.length() - 2);
            return Long.parseLong(removeLast2Digit);
        }else{
            replaceDot = replaceComma.replace(".", "");
            return Long.parseLong(replaceDot);
        }
    }

    static void MakeLogTxtCustom(String result, String tipeAPI) throws IOException{
        Date newDate = new Date();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(newDate);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DATE);
        int hour = calendar.get(Calendar.HOUR);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);
        String createdOn = "Created ON : Date(YYYY/MM/DD): "+year+"-"+(month+1)+"-"+day+", Time: "+hour+":"+minute+":"+second;

        File directoryPath = new File("").getAbsoluteFile();

        String log_date = year + "_" + month + "_" + day + "_";

        FileWriter csvWriter = new FileWriter(directoryPath + "//log_"+year+"_"+(month+1)+".txt", true);
        csvWriter.write(result + ". " + tipeAPI);
        csvWriter.write("\n");

        csvWriter.flush();
        csvWriter.close();
    }

    static void MakeLogTxt(String result, String tipeAPI) throws IOException{
        Date newDate = new Date();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(newDate);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DATE);
        int hour = calendar.get(Calendar.HOUR);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);
        String createdOn = "Created ON : Date(YYYY/MM/DD): "+year+"-"+(month+1)+"-"+day+", Time: "+hour+":"+minute+":"+second;

        File directoryPath = new File("").getAbsoluteFile();

        String log_date = year + "_" + month + "_" + day + "_";

        FileWriter csvWriter = new FileWriter(directoryPath + "//log_"+year+"_"+(month+1)+".txt", true);
        csvWriter.write("==================================================");
        csvWriter.write("\n");
        csvWriter.write(tipeAPI);
        csvWriter.write("\n");
        csvWriter.write(createdOn);
        csvWriter.write("\n");
        csvWriter.write(result);
        csvWriter.write("\n");

        csvWriter.flush();
        csvWriter.close();
    }




}

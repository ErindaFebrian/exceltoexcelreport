package com.mega.Excel.Dto;


import org.eclipse.persistence.jpa.jpql.parser.DateTime;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

/**
 * @author Erinda Febrian - erindafebrian3@gmail.com
 * 29/09/2020
 * class name DataDto
 */


public class DataDto {
    private Integer id;
    private String typeCase;
    private String cardnumber;
    private String expDate;
    private String custNo;
    private String alamat1Rmh;
    private String alamat2Rmh;
    private String alamat3Rmh;
    private String alamat4Rmh;
    private String kotaRmh;
    private String kodeposRmh;
    private String alamat1Kntr;
    private String alamat2Kntr;
    private String alamat3Kntr;
    private String alamat4Kntr;
    private String kotaKntr;
    private String kodeposKntr;
    private String namaKantor;
    private String email;
    private String tempCredLmt;
    private String effectiveDateTo;
    private String cardAddress;
    private String flagStatement;
    private String flagRpa;
    private String descError;
    private Date createDate;
    private String refId;
    private String reason;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTypeCase() {
        return typeCase;
    }

    public void setTypeCase(String typeCase) {
        this.typeCase = typeCase;
    }

    public String getCardnumber() {
        return cardnumber;
    }

    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getCustNo() {
        return custNo;
    }

    public void setCustNo(String custNo) {
        this.custNo = custNo;
    }

    public String getAlamat1Rmh() {
        return alamat1Rmh;
    }

    public void setAlamat1Rmh(String alamat1Rmh) {
        this.alamat1Rmh = alamat1Rmh;
    }

    public String getAlamat2Rmh() {
        return alamat2Rmh;
    }

    public void setAlamat2Rmh(String alamat2Rmh) {
        this.alamat2Rmh = alamat2Rmh;
    }

    public String getAlamat3Rmh() {
        return alamat3Rmh;
    }

    public void setAlamat3Rmh(String alamat3Rmh) {
        this.alamat3Rmh = alamat3Rmh;
    }

    public String getAlamat4Rmh() {
        return alamat4Rmh;
    }

    public void setAlamat4Rmh(String alamat4Rmh) {
        this.alamat4Rmh = alamat4Rmh;
    }

    public String getKotaRmh() {
        return kotaRmh;
    }

    public void setKotaRmh(String kotaRmh) {
        this.kotaRmh = kotaRmh;
    }

    public String getKodeposRmh() {
        return kodeposRmh;
    }

    public void setKodeposRmh(String kodeposRmh) {
        this.kodeposRmh = kodeposRmh;
    }

    public String getAlamat1Kntr() {
        return alamat1Kntr;
    }

    public void setAlamat1Kntr(String alamat1Kntr) {
        this.alamat1Kntr = alamat1Kntr;
    }

    public String getAlamat2Kntr() {
        return alamat2Kntr;
    }

    public void setAlamat2Kntr(String alamat2Kntr) {
        this.alamat2Kntr = alamat2Kntr;
    }

    public String getAlamat3Kntr() {
        return alamat3Kntr;
    }

    public void setAlamat3Kntr(String alamat3Kntr) {
        this.alamat3Kntr = alamat3Kntr;
    }

    public String getAlamat4Kntr() {
        return alamat4Kntr;
    }

    public void setAlamat4Kntr(String alamat4Kntr) {
        this.alamat4Kntr = alamat4Kntr;
    }

    public String getKotaKntr() {
        return kotaKntr;
    }

    public void setKotaKntr(String kotaKntr) {
        this.kotaKntr = kotaKntr;
    }

    public String getKodeposKntr() {
        return kodeposKntr;
    }

    public void setKodeposKntr(String kodeposKntr) {
        this.kodeposKntr = kodeposKntr;
    }

    public String getNamaKantor() {
        return namaKantor;
    }

    public void setNamaKantor(String namaKantor) {
        this.namaKantor = namaKantor;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTempCredLmt() {
        return tempCredLmt;
    }

    public void setTempCredLmt(String tempCredLmt) {
        this.tempCredLmt = tempCredLmt;
    }

    public String getEffectiveDateTo() {
        return effectiveDateTo;
    }

    public void setEffectiveDateTo(String effectiveDateTo) {
        this.effectiveDateTo = effectiveDateTo;
    }

    public String getCardAddress() {
        return cardAddress;
    }

    public void setCardAddress(String cardAddress) {
        this.cardAddress = cardAddress;
    }

    public String getFlagStatement() {
        return flagStatement;
    }

    public void setFlagStatement(String flagStatement) {
        this.flagStatement = flagStatement;
    }

    public String getFlagRpa() {
        return flagRpa;
    }

    public void setFlagRpa(String flagRpa) {
        this.flagRpa = flagRpa;
    }

    public String getDescError() {
        return descError;
    }

    public void setDescError(String descError) {
        this.descError = descError;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}

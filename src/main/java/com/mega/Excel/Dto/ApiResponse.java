package com.mega.Excel.Dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParseException;

import java.io.IOException;

/**
 * @author Erinda Febrian - erindafebrian3@gmail.com
 * 30/09/2020
 * class name ApiResponse
 */


public class ApiResponse {
    @JsonInclude(value = JsonInclude.Include.ALWAYS)
    private Boolean success;

    @JsonInclude(value = JsonInclude.Include.ALWAYS)
    private String message;

    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private Object data;

    private int specificCode;

    public ApiResponse(Boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public ApiResponse(Boolean success, String message, Object obj) {
        this.success = success;
        this.message = message;
        this.setData(obj);
    }

    public ApiResponse(Boolean success, String message, Object obj, int specificCode) {
        this.success = success;
        this.message = message;
        this.specificCode = specificCode;
        this.setData(obj);
    }

    public ApiResponse(Object obj) {
        if(obj == null) {
            this.success = false;
            this.message = "failed";
        }
        else {
            this.success = true;
            this.message = "success";
        }

        this.setData(obj);
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

//    public static ApiResponse ofResponse(String response) throws JsonParseException, JsonMappingException, IOException {
//        ObjectMapper mapper = new ObjectMapper();
//        JavaTimeModule javaTimeModule = new JavaTimeModule();
//        javaTimeModule.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss")));
//
//        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
//        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//        mapper.registerModule(javaTimeModule);
//        mapper.findAndRegisterModules();
//
//        //Replace null json value to empty string value
//        String sdata = response.replace(":null", ":\"\"");
//
//        ApiResponse result = mapper.readValue(sdata, ApiResponse.class);
//        return result;
//        return null;
//    }

    public String toString() {
//        ObjectMapper mapper = new ObjectMapper();
//        JavaTimeModule javaTimeModule = new JavaTimeModule();
//        javaTimeModule.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss")));
//
//        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
//        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//        mapper.registerModule(javaTimeModule);
//        mapper.findAndRegisterModules();
//
//        //Produce string value
//        String sdata = null;
//        try {
//            sdata = mapper.writeValueAsString(this);
//        } catch (JsonProcessingException e) {
//            e.printStackTrace();
//        }
//        return sdata;
        return null;
    }

    public int getSpecificCode() {
        return specificCode;
    }

    public void setSpecificCode(int specificCode) {
        this.specificCode = specificCode;
    }
}

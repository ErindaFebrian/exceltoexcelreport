package com.mega.Excel.Model;

import lombok.Data;
import org.eclipse.persistence.jpa.jpql.parser.DateTime;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Erinda Febrian - erindafebrian3@gmail.com
 * 30/09/2020
 * class name DataEntity
 */

@Entity
@Data
@Table(name= "tm_msmile_api")
public class DataEntity implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(length = 11)
    private Integer id;

    private String typeCase;

    @Column (length = 50)
    private String cardnumber;

    @Column (length = 50)
    private String expDate;

    @Column (length = 50)
    private String custNo;

    private String alamat1Rmh;
    private String alamat2Rmh;
    private String alamat3Rmh;
    private String alamat4Rmh;
    private String kotaRmh;
    private String kodeposRmh;
    private String alamat1Kntr;
    private String alamat2Kntr;
    private String alamat3Kntr;
    private String alamat4Kntr;
    private String kotaKntr;
    private String kodeposKntr;
    private String namaKantor;
    private String email;
    private String tempCredLmt;
    private String effectiveDateTo;
    private String cardAddress;
    private String flagStatement;
    private String flagRpa;
    private String descError;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    private String refId;
    private String reason;
}
